from controller_interface import ControllerInterface
import numpy
import copy
import random
from random import randint

class Controller(ControllerInterface):
    def vec_module(self, v):
        sum2 = 0.0
        for component in v:
            sum2 = sum2 + component ** 2
        return numpy.sqrt(sum2)

    def sign(self, banana):
        if banana < 0:
            return -1
        else:
            return 1

    def direction(self, a, c, b) -> int:
        a_point = [a.x, a.y]
        b_point = [b.x, b.y]
        c_point = [c.x, c.y]

        # Transmute list to numpy ndarray:
        a_point = numpy.array(a_point, float)
        b_point = numpy.array(b_point, float)
        c_point = numpy.array(c_point, float)

        # Define vectors:
        cb_vector = c_point - b_point
        ba_vector = b_point - a_point
        ca_vector = c_point - a_point

        # Calculate moduli:
        ba_modulus = self.vec_module(ba_vector)
        ca_modulus = self.vec_module(ca_vector)

        biggest_vector = ba_vector if ba_modulus > ca_modulus else ca_vector
        if abs(ba_modulus - ca_modulus) < 0.000001:  # Numeric error handling.
            biggest_vector = ba_vector

        # Calculate determinant:
        determinant = numpy.linalg.det([[cb_vector[0], cb_vector[1]], [biggest_vector[0], biggest_vector[1]]])

        # Print result
        signal = self.sign(determinant)
        #print(signal)
        return signal




    def take_action(self, weights: tuple) -> int:
        """
        :return: An integer corresponding to an action:
        1 - Right
        2 - Left
        3 - Accelerate forward
        4 - Discharge 
        5 - Nothing
        """

        #(feat_go_go_go, feat_a_left, feat_a_right, feat_em_left, feat_em_right, feat_m_left, feat_m_right,
        # feat_need_gas, feat_pls_die, feat_steal)

        features = self.compute_features(self.sensors)

        # f01: left
        feature01 = features[0]
        # f02: right
        feature02 = features[1]
        # f03: acc
        feature03 = features[2]
        # f04: discharge
        feature04 = features[3]
        # f05: do nothing
        feature05 = features[4]

        choose_r = weights[0] + weights[5] * feature05 + weights[10] * feature02
        choose_l = weights[1] + weights[6] * feature05 + weights[11] * feature01
        choose_a = weights[2] + weights[7] * feature05 + weights[12] * feature03
        choose_d = weights[3] + weights[8] * feature05
        choose_n = weights[4] + weights[9] * feature05


        choices = [choose_r, choose_l, choose_a, choose_d, choose_n]

        max_value_index = 0
        max_value = 0
        for i, item in enumerate(choices):
            if choices[i] > max_value:
                max_value = choices[i]
                max_value_index = i

        # print("choice: " + str(max_value_index + 1))
        return max_value_index + 1

    def compute_features(self, sensors: dict) -> tuple:
        """
        This function should take the raw sensor information of the ship (see below) and compute useful features
        for selecting an action.
        The ship has the following sensors:

        :param sensors: contains:
             'asteroid_position': (x, y)
             'asteroid_velocity': (n, m)
             'asteroid_resources': 0 - ???
             'align_asteroid': 0 or 1
             'align_mothership': 0 or 1
             'drill_angle': angle in rad
             'drill_position': (x, y)
             'drill_velocity': (n, m)
             'drill_mothership_position': (x, y)
             'drill_resources': 0 - ???
             'drill_touching_asteroid': 0 or 1
             'drill_touching_mothership': 0 or 1
             'drill_discharge_cooldown': 0 - COOLDOWN (defined in config.py)
             'drill_edge_position': (x, y)
             'drill_gas': 0 - MAX_GAS (defined in config.py)
             'enemy_1_drill_angle': angle in rad
             'enemy_1_drill_position': (x, y)
             'enemy_1_drill_velocity': (n, m)
             'enemy_1_drill_mothership_position': (x, y)
             'enemy_1_drill_resources': 0 - 0
             'enemy_1_drill_touching_asteroid': 0 or 1
             'enemy_1_drill_touching_mothership': 0 or 1
             'enemy_1_drill_discharge_cooldown': 0 - COOLDOWN  (defined in config.py)
             'enemy_1_drill_edge_position': (x, y)
             'enemy_1_drill_gas': 0 - MAX_GAS (defined in config.py)
        :return: A Tuple containing the features you defined
        """

        # *****
        # func: NORMALIZATION
        # doc: provides the normal form of a parameter
        #      given its maximum and minimum value
        # *****
        def normalization(param, min, max):
            return (param - min) / (max - min)

        num_pi = 3.14159265359
        enemy_losing_gas = 0
        steal = 0
        asteroid_left = 0
        asteroid_right = 0
        asteroid_aligned = 0
        mship_aligned = 0
        e_mship_aligned = 0
        out_of_gas = 0
        mship_left = 0
        mship_right = 0
        e_mship_left = 0
        e_mship_right = 0
        touching_a = sensors['drill_touching_asteroid']
        touching_m = sensors['drill_touching_mothership']


        if(sensors['enemy_1_drill_position'] == sensors['drill_mothership_position']) and (
                sensors['enemy_1_drill_touching_mothership']):
            enemy_stealing = 1

        if self.direction(sensors['asteroid_position'], sensors['drill_position'], sensors['drill_edge_position']) == -1:
            asteroid_left = 1

        if self.direction(sensors['asteroid_position'], sensors['drill_position'], sensors['drill_edge_position']) == 1:
            asteroid_right = 1

        if sensors['align_asteroid']:
            asteroid_right = 0
            asteroid_left = 0
            asteroid_aligned = 1

        if self.direction(sensors['drill_mothership_position'], sensors['drill_position'], sensors['drill_edge_position']) == -1:
            mship_left = 1

        if self.direction(sensors['drill_mothership_position'], sensors['drill_position'], sensors['drill_edge_position']) == 1:
            mship_right = 1

        if sensors['align_mothership']:
            mship_right = 0
            mship_left = 0
            mship_aligned = 1

        if self.direction(sensors['enemy_1_drill_mothership_position'], sensors['drill_position'], sensors['drill_edge_position']) == -1:
            e_mship_left = 1

        if self.direction(sensors['enemy_1_drill_mothership_position'], sensors['drill_position'], sensors['drill_edge_position']) == 1:
            e_mship_right = 1

        if sensors['align_enemy_mothership']:
            e_mship_right = 0
            e_mship_left = 0
            e_mship_aligned = 1

        if (20 < sensors['drill_gas'] < 60):
            out_of_gas = 1 - normalization(sensors['drill_gas'], 20, 60)

        if (sensors['drill_gas'] <= 20):
            out_of_gas = 1

        if (20 < sensors['enemy_1_drill_gas'] < 45):
            enemy_losing_gas = 1 - normalization(sensors['drill_gas'], 20, 45)

        if (sensors['enemy_1_drill_gas'] <= 20):
            enemy_losing_gas = 1

        if (20 < sensors['enemy_1_drill_discharge_cooldown'] < 100):
            steal = normalization(sensors['enemy_1_drill_discharge_cooldown'], 20, 100 )


        # FEATURES

        # about moviment:

        # f01: left
        feature01 = ((asteroid_left + mship_left + e_mship_left) - (asteroid_right + mship_right + e_mship_right))/3
        # f02: right
        feature02 = ((asteroid_right + mship_right + e_mship_right) - (asteroid_left + mship_left + e_mship_left))/3
        # f03: acc
        feature03 = ((asteroid_aligned + mship_aligned + e_mship_aligned) - (touching_m + touching_a))/3
        # f04: discharge
        feature04 = (enemy_losing_gas + steal)/2
        # f05: do nothing
        feature05 = (touching_a + touching_m)/2

        # print(tuple([feature01, feature02, feature03, feature04, feature05]))

        return tuple([feature01, feature02, feature03, feature04, feature05])
        
    def learn(self, weights: tuple):
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE

        HINTS: You can call self.run_episode (see controller_interface.py) to evaluate a given set of weights.
                The variable self.episode shows the number of times run_episode method has been called 

        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """
        class Individual:
		
            def __init__ (self, genes):
                self.genes = genes
                self.fitness = 0
                
            def getGene (self, index):
                return self.genes[index]
                
            def setGene (self, index, gene):
                self.genes[index] = gene
            
            def getFitness (self):
                return self.fitness
            
            def setFitness (self, fitness):
                self.fitness = fitness
                
            def getGenesLength(self):
                return len(self.genes)
                
        class Population:
            
            def __init__ (self, sizePopulation, generate):
                self.individuals = []
                alpha = random.uniform(-0.1,0.1)
                if generate:
                    for x in range(0, sizePopulation):
                        y = []
                      #  y = [random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random()]
                        for w in weights:
                            rand_num = randint(0, 1)
                            if rand_num == 0:
                                y.append(w + alpha)
                            elif rand_num == 1:
                                y.append(w)
                        newIndividual = Individual(y)
                        self.individuals.append(newIndividual)
                    
                    
            def getIndividual (self, index):
                return self.individuals[index]
            
            def populationSize(self):
                return len(self.individuals)
                
            def getBestIndividual(self):
                best = self.individuals[0]
                
                for individual in self.individuals:
                    if individual.getFitness() > best.getFitness():
                        best = individual
                        
                return best
                
        class GeneticAlgorithm:
            
            
            def crossover(self, individual1, individual2):
                length = individual1.getGenesLength()
                initialGenes = [0] * length
                newIndividual = Individual(initialGenes)
                for x in range(0, (int)(length/2)):
                    newIndividual.setGene(x, individual1.getGene(x))
                for x in range ((int)(length/2), length):
                    newIndividual.setGene(x, individual2.getGene(x))
                    
                return newIndividual
                
            def mutate (self, individual):
                length = individual.getGenesLength()
                index = randint(0, length-1)
                alpha = 1 - individual.getGene(index)
                newGene = individual.getGene(index) + (alpha*2)/10
                individual.setGene(index, newGene)
            
            def tournament(self, population, tSize):
                tourn = Population(tSize, 0)
                for x in range(0, tSize):
                    index1 = randint(0, population.populationSize() -1)
                    tourn.individuals.append(population.getIndividual(index1))
                ind = tourn.getBestIndividual()
                
                return ind
                
            def evolve(self, population):
                mutationRate = 0.1
                popSize = population.populationSize()
                newPopulation = Population(popSize, 0)
                ind = population.getBestIndividual()
                newPopulation.individuals.append(ind)
                for x in range(1, popSize):
                    individual1 = self.tournament(population, (int)(popSize/10))
                    individual2 = self.tournament(population, (int)(popSize/10))
                    firstNewInd = self.crossover(individual1, individual2)
                    if (random.uniform(0,1) < mutationRate):
                        self.mutate(firstNewInd)
                    secNewInd = self.crossover(individual2, individual1)
                    if (random.uniform(0,1) < mutationRate):
                        self.mutate(secNewInd)
                    rand = randint(0,1)
                    if rand == 0:
                        newInd = firstNewInd
                    else:
                        newInd = secNewInd
                    newPopulation.individuals.append(newInd)
                return newPopulation
        
        pop = Population(50, 1)
        alg = GeneticAlgorithm()
        for x in range(0, pop.populationSize()-1):
            fitness = self.run_episode(pop.individuals[x].genes)
            pop.individuals[x].setFitness(fitness)
        best = pop.getBestIndividual()
        pop = alg.evolve(pop)
        for x in range(0, pop.populationSize()-1):
                    fitness = self.run_episode(pop.individuals[x].genes)
                    pop.individuals[x].setFitness(fitness)
        newBest = pop.getBestIndividual()
        while True:
            if (best.getFitness() > newBest.getFitness()):
                return best.genes
            else:
                print(best.genes)
                print(best.getFitness())
                print (str(self.episode))
                best = pop.getBestIndividual()
                pop = alg.evolve(pop)
                for x in range(0, pop.populationSize()-1):
                    fitness = self.run_episode(pop.individuals[x].genes)
                    pop.individuals[x].setFitness(fitness)
                newBest = pop.getBestIndividual()
