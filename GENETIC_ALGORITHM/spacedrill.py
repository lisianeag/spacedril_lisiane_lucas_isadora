import pygame


from simulation import Simulation
from gui import GUI
from controller1.controller import Controller as ControllerP1
from controller2.controller import Controller as ControllerP2
from bots import RandomBot, ParasiteBot, CustomBot, CollectorBot
from random import uniform
from parser import parser
from numpy import loadtxt
import config

import time


def play():
    s = Simulation()
    gui = GUI(s)
    c = ControllerP1(s)
    while True:
        events = pygame.event.get()
        action = 6
        action_2 = 6

        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    exit()
                if event.key == pygame.K_UP:
                    action = s.drill_1.ACCELERATE
                if event.key == pygame.K_LEFT:
                    action = s.drill_1.LEFT
                if event.key == pygame.K_RIGHT:
                    action = s.drill_1.RIGHT
                if event.key == pygame.K_SPACE:
                    action = s.drill_1.ATTACH
                if event.key == pygame.K_f:
                    action = s.drill_1.DISCHARGE

                if event.key == pygame.K_w:
                    action_2 = s.drill_2.ACCELERATE
                if event.key == pygame.K_a:
                    action_2 = s.drill_2.LEFT
                if event.key == pygame.K_d:
                    action_2 = s.drill_2.RIGHT
                if event.key == pygame.K_e:
                    action_2 = s.drill_2.DISCHARGE

                if event.key == pygame.K_r:
                    s.reset()

        s.frame_step(action, action_2)
        print(s.drill_1.sensors)

        gui.draw()
        time.sleep(1/60)


def learn(controller_1, controller_2, w1, w2):
    controller_1.set_controller_2_for_learn_mode(controller_2, w2)
    controller_1.learn(w1)


def learn_gui(controller_1, controller_2, w1, w2, sim):
    gui = GUI(sim)

    controller_1.set_gs(gui)
    controller_1.set_controller_2_for_learn_mode(controller_2, w2)
    controller_1.learn(w1)


def evaluate(controller_1, controller_2, w1, w2, sim):

    gui = GUI(sim)

    for i in range(0, config.EPISODE_LEN):
        controller_1.sensors = s.drill_1.sensors
        controller_2.sensors = s.drill_2.sensors
        action_1 = controller_1.take_action(w1)
        action_2 = controller_2.take_action(w2)
        s.frame_step(action_1, action_2)
        gui.draw()
        time.sleep(1/60)
    pass


if __name__ == '__main__':
    args, leftovers = parser()

    s = Simulation()

    if args.w1 is None:
        weights_p1 = [uniform(-1, 1) for i in range(0, 100)]
    else:
        weights_p1 = loadtxt(args.w1[0])

    if args.w2 is None:
        weights_p2 = [uniform(-1, 1) for i in range(0, 100)]
    else:
        weights_p2 = loadtxt(args.w2[0])

    if args.p1 is None:
        player_1 = ControllerP1(s)
    elif args.p1[0] == 'controller1':
        player_1 = ControllerP1(s)
    elif args.p1[0] == 'controller2':
        player_1 = ControllerP2(s)
    elif args.p1[0] == 'random_bot':
        player_1 = RandomBot(s)
    elif args.p1[0] == 'parasite_bot':
        player_1 = ParasiteBot(s)
    elif args.p1[0] == 'custom_bot':
        player_1 = CustomBot(s)
    elif args.p1[0] == 'collector_bot':
        player_1 = CollectorBot(s)
    else:
        player_1 = ControllerP1(s)

    if args.p2 is None:
        player_2 = RandomBot(s)
    elif args.p2[0] == 'controller1':
        player_2 = ControllerP1(s)
    elif args.p2[0] == 'controller2':
        player_2 = ControllerP2(s)
    elif args.p2[0] == 'random_bot':
        player_2 = RandomBot(s)
    elif args.p2[0] == 'parasite_bot':
        player_2 = ParasiteBot(s)
    elif args.p2[0] == 'custom_bot':
        player_2 = CustomBot(s)
    elif args.p2[0] == 'collector_bot':
        player_2 = CollectorBot(s)
    else:
        player_2 = RandomBot(s)

    if str(args.mode) == 'learn':
        learn(player_1, player_2, weights_p1, weights_p2)
    elif str(args.mode) == 'learn_gui':
        learn_gui(player_1, player_2, weights_p1, weights_p2, s)
    elif str(args.mode) == 'play':
        play()
    elif str(args.mode) == 'evaluate':
        evaluate(player_1, player_2, weights_p1, weights_p2, s)
