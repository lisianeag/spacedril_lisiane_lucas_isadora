from controller_interface import ControllerInterface
import random
import numpy

class Controller(ControllerInterface):

    def vec_module(self, v):
        sum2 = 0.0
        for component in v:
            sum2 = sum2 + component ** 2
        return numpy.sqrt(sum2)

    def sign(self, banana):
        if banana < 0:
            return -1
        else:
            return 1

    def direction(self, a, c, b) -> int:
        a_point = [a.x, a.y]
        b_point = [b.x, b.y]
        c_point = [c.x, c.y]

        # Transmute list to numpy ndarray:
        a_point = numpy.array(a_point, float)
        b_point = numpy.array(b_point, float)
        c_point = numpy.array(c_point, float)

        # Define vectors:
        cb_vector = c_point - b_point
        ba_vector = b_point - a_point
        ca_vector = c_point - a_point

        # Calculate moduli:
        ba_modulus = self.vec_module(ba_vector)
        ca_modulus = self.vec_module(ca_vector)

        biggest_vector = ba_vector if ba_modulus > ca_modulus else ca_vector
        if abs(ba_modulus - ca_modulus) < 0.000001:  # Numeric error handling.
            biggest_vector = ba_vector

        # Calculate determinant:
        determinant = numpy.linalg.det([[cb_vector[0], cb_vector[1]], [biggest_vector[0], biggest_vector[1]]])

        # Print result
        signal = self.sign(determinant)
        #print(signal)
        return signal




    def take_action(self, weights: tuple) -> int:
        """
        :return: An integer corresponding to an action:
        1 - Right
        2 - Left
        3 - Accelerate forward
        4 - Discharge 
        5 - Nothing
        """

        #(feat_go_go_go, feat_a_left, feat_a_right, feat_em_left, feat_em_right, feat_m_left, feat_m_right,
        # feat_need_gas, feat_pls_die, feat_steal)

        features = self.compute_features(self.sensors)

        # f01: left
        feature01 = features[0]
        # f02: right
        feature02 = features[1]
        # f03: acc
        feature03 = features[2]
        # f04: discharge
        feature04 = features[3]
        # f05: do nothing
        feature05 = features[4]

        choose_r = weights[0] + weights[5] * feature05 + weights[10] * feature02
        choose_l = weights[1] + weights[6] * feature05 + weights[11] * feature01
        choose_a = weights[2] + weights[7] * feature05 + weights[12] * feature03
        choose_d = weights[3] + weights[8] * feature05 + weights[13] * feature04
        choose_n = weights[4] + weights[9] * feature05



        choices = [choose_r, choose_l, choose_a, choose_d, choose_n]

        max_value_index = 0
        max_value = 0
        for i, item in enumerate(choices):
            if choices[i] > max_value:
                max_value = choices[i]
                max_value_index = i

        # print("choice: " + str(max_value_index + 1))
        return max_value_index + 1

    def compute_features(self, sensors: dict) -> tuple:
        """
        This function should take the raw sensor information of the ship (see below) and compute useful features
        for selecting an action.
        The ship has the following sensors:

        :param sensors: contains:
             'asteroid_position': (x, y)
             'asteroid_velocity': (n, m)
             'asteroid_resources': 0 - ???
             'align_asteroid': 0 or 1
             'align_mothership': 0 or 1
             'drill_angle': angle in rad
             'drill_position': (x, y)
             'drill_velocity': (n, m)
             'drill_mothership_position': (x, y)
             'drill_resources': 0 - ???
             'drill_touching_asteroid': 0 or 1
             'drill_touching_mothership': 0 or 1
             'drill_discharge_cooldown': 0 - COOLDOWN (defined in config.py)
             'drill_edge_position': (x, y)
             'drill_gas': 0 - MAX_GAS (defined in config.py)
             'enemy_1_drill_angle': angle in rad
             'enemy_1_drill_position': (x, y)
             'enemy_1_drill_velocity': (n, m)
             'enemy_1_drill_mothership_position': (x, y)
             'enemy_1_drill_resources': 0 - 0
             'enemy_1_drill_touching_asteroid': 0 or 1
             'enemy_1_drill_touching_mothership': 0 or 1
             'enemy_1_drill_discharge_cooldown': 0 - COOLDOWN  (defined in config.py)
             'enemy_1_drill_edge_position': (x, y)
             'enemy_1_drill_gas': 0 - MAX_GAS (defined in config.py)
        :return: A Tuple containing the features you defined
        """

        # *****
        # func: NORMALIZATION
        # doc: provides the normal form of a parameter
        #      given its maximum and minimum value
        # *****
        def normalization(param, min, max):
            return (param - min) / (max - min)

        num_pi = 3.14159265359
        enemy_losing_gas = 0
        steal = 0
        asteroid_left = 0
        asteroid_right = 0
        asteroid_aligned = 0
        mship_aligned = 0
        e_mship_aligned = 0
        out_of_gas = 0
        mship_left = 0
        mship_right = 0
        e_mship_left = 0
        e_mship_right = 0
        touching_a = sensors['drill_touching_asteroid']
        touching_m = sensors['drill_touching_mothership']


        if(sensors['enemy_1_drill_position'] == sensors['drill_mothership_position']) and (
                sensors['enemy_1_drill_touching_mothership']):
            enemy_stealing = 1

        if self.direction(sensors['asteroid_position'], sensors['drill_position'], sensors['drill_edge_position']) == -1:
            asteroid_left = 1

        if self.direction(sensors['asteroid_position'], sensors['drill_position'], sensors['drill_edge_position']) == 1:
            asteroid_right = 1

        if sensors['align_asteroid']:
            asteroid_right = 0
            asteroid_left = 0
            asteroid_aligned = 1

        if self.direction(sensors['drill_mothership_position'], sensors['drill_position'], sensors['drill_edge_position']) == -1:
            mship_left = 1

        if self.direction(sensors['drill_mothership_position'], sensors['drill_position'], sensors['drill_edge_position']) == 1:
            mship_right = 1

        if sensors['align_mothership']:
            mship_right = 0
            mship_left = 0
            mship_aligned = 1

        if self.direction(sensors['enemy_1_drill_mothership_position'], sensors['drill_position'], sensors['drill_edge_position']) == -1:
            e_mship_left = 1

        if self.direction(sensors['enemy_1_drill_mothership_position'], sensors['drill_position'], sensors['drill_edge_position']) == 1:
            e_mship_right = 1

        if sensors['align_enemy_mothership']:
            e_mship_right = 0
            e_mship_left = 0
            e_mship_aligned = 1

        if (20 < sensors['drill_gas'] < 60):
            out_of_gas = 1 - normalization(sensors['drill_gas'], 20, 60)

        if (sensors['drill_gas'] <= 20):
            out_of_gas = 1

        if (20 < sensors['enemy_1_drill_gas'] < 45):
            enemy_losing_gas = 1 - normalization(sensors['drill_gas'], 20, 45)

        if (sensors['enemy_1_drill_gas'] <= 20):
            enemy_losing_gas = 1

        if (20 < sensors['enemy_1_drill_discharge_cooldown'] < 100):
            steal = normalization(sensors['enemy_1_drill_discharge_cooldown'], 20, 100 )


        # FEATURES

        # about moviment:

        # f01: left
        feature01 = (((asteroid_left * (1 - out_of_gas)) + (mship_left * out_of_gas))
                     - (asteroid_right * (1 - out_of_gas) + mship_right * out_of_gas))/2
        # f02: right
        feature02 = (((asteroid_right * (1 - out_of_gas)) + (mship_right * out_of_gas))
                     - (asteroid_left * (1 - out_of_gas) + mship_left * out_of_gas))/2
        # f03: acc
        feature03 = ((asteroid_aligned * (1 - out_of_gas) + mship_aligned * out_of_gas)
                     - (touching_m + touching_a))/2
        # f04: discharge
        feature04 = enemy_losing_gas
        # f05: do nothing
        feature05 = (touching_a + touching_m)/2


        #print(tuple([feature01, feature02, feature03, feature04, feature05]))
        return tuple([feature01, feature02, feature03, feature04, feature05])

        # *****
        # func: NEIGHBORS
        # doc: provides random neighbors
        #      given a list of weights
        # *****

    def neighbors(self, weights_list, max, alpha):
        neighbors_list = []
        for i in range(0, max):
            new_neighbor = list(weights_list)
            random_neighbor = []
            for j in range(0, len(new_neighbor) - 1):
                rand_num = random.randint(0, 2)
                if rand_num == 0:
                    random_neighbor.append(new_neighbor[j] + alpha)
                elif rand_num == 1:
                    random_neighbor.append(new_neighbor[j] - alpha)
                elif rand_num == 2:
                    random_neighbor.append(new_neighbor[j])
            neighbors_list.append(tuple(random_neighbor))
        return neighbors_list

    def learn(self, weights: tuple):
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE

        HINTS: You can call self.run_episode (see controller_interface.py) to evaluate a given set of weights.
               The variable self.episode shows the number of times run_episode method has been called

        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """

        # HILL CLIMBING
        current_weights = weights[:14]
        alpha = 0.05
        max_neighbors = 50
        print("Valores iniciais" + str(current_weights))
        while True:
            # neighborhood
            neighborhood = self.neighbors(weights, max_neighbors, alpha)
            # best neighbor = current state
            current_best = current_weights

            # for each neighbor in neighborhood
            for neighbor in neighborhood:
                current_best_value = self.run_episode(current_best)
                neighbor_value = self.run_episode(neighbor)

                # if current neighbor value better than best neighbor value
                if (neighbor_value > current_best_value):
                    # best neighbor = current neighbor
                    current_best = neighbor
                    print("Melhor valor (vizinho): " + str(neighbor_value))
                    print("Episodios: " + str(self.episode))

            current_best_value = self.run_episode(current_best)
            current_weights_value = self.run_episode(current_weights)
            # if best neighbor value better than current state
            if (current_best_value > current_weights_value):
                # current state = best neighbor
                current_weights = current_best
                print("Melhor valor: " + str(current_best_value))
                print("Episodios: " + str(self.episode))

            # else: cant find a better neighbour
            else:
                print("Melhor pack de pesos atualmente: " + str(current_weights))
                weights = [random.uniform(0, 1) for _ in range(15)]
                print("Novo pack de pesos" + str(weights))
                print("E o melhor pack de pesos no geral: " + str(current_weights))
